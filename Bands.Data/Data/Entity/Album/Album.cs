﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bands.Data
{
    [Table("Albums")]
    public class Album
    {

        [Key]
        public Guid Id { get; set; }

        [Required]
        [MaxLength(200)]
        public string Title { get; set; }


        [MaxLength(400)]
        public string Description { get; set; }

        [ForeignKey("BandId")]

        public Guid BandId { get; set; }

        public Band band { get; set; }
    }
}
