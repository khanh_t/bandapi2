﻿using AutoMapper;
using Bands.Business;
using Bands.Services;
using Bands.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Bands.Common;

namespace Bands.Controllers
{
    [ApiController]
    [Route("api/v1/bands")]
    public class BandsController : ControllerBase
    {
        private readonly IBandHandler _bandRepository;

        public BandsController(IBandHandler bandRepository)
        {

            _bandRepository = bandRepository ??
          throw new ArgumentNullException(nameof(bandRepository));

        }

        /// <summary>
        /// lấy band theo pagination
        /// </summary>
        /// <param name="page">Số thứ tự trang tìm kiếm</param>
        /// <param name="size">Số bản ghi giới hạn một trang</param>
        /// <param name="filter">Thông tin lọc nâng cao (Object Json)</param>
        /// <param name="sort">Thông tin sắp xếp (Array Json)</param>
        /// <returns></returns>
        [HttpGet(Name = "Getbands")]

        public async Task<IActionResult> GetBands([FromQuery] int size = 20, [FromQuery] int page = 1, [FromQuery] string filter = "{}", [FromQuery] string sort = "")
        {
            var filterObject = JsonConvert.DeserializeObject<BandQueryModel>(filter);
            filterObject.Sort = sort != null ? sort : filterObject.Sort;
            filterObject.Size = size;
            filterObject.Page = page;
            var result = await _bandRepository.GetPageAsync(filterObject);

            return Helper.TransformData(result);
        }

        /// <summary>
        /// Lấy band theo id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}", Name = "Getband")]
        public async Task<IActionResult> GetBand(Guid id)
        {
            var result = await _bandRepository.GetById(id);
            return Helper.TransformData(result);
        }

        /// <summary>
        /// Thêm 1 band
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreatedBand([FromBody] BandCreatingUpdateModel dto)
        {
            var result = await _bandRepository.CreateBand(dto);
            return Helper.TransformData(result);
        }

        /// <summary>
        /// Sửa một Band theo bandId
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateBand(Guid id, [FromBody] BandCreatingUpdateModel dto)
        {

            var result = await _bandRepository.UpdateBand(dto, id);
            return Helper.TransformData(result);
        }

        /// <summary>
        /// Xóa Band
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteBand(Guid id)
        {

            var result = await _bandRepository.DeleteBand(id);
            return Helper.TransformData(result);

        }

    }
}
