﻿using Inmergers.Common;
using Microsoft.Extensions.Configuration;


namespace Bands.Common
{
    public partial class Utils
    {
        public static string GetConfig(string code)
        {
            IConfigurationRoot configuration = ConfigCollection.Instance.GetConfiguration();
            var value = configuration[code];
            return value;
        }
    }
}