﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;


namespace Bands.Common
{
    public class TreeItem<T>
    {
        public T Item { get; set; }
        public List<TreeItem<T>> Children { get; set; }
    }
    public static class ExtensionMethods
    {
        public static object GetPropValue(this object src, string propName)
        {
            return src.GetType().GetProperty(propName)?.GetValue(src, null);
        }

        public static bool HasProperty(this object obj, string propertyName)
        {
            return obj.GetType().GetProperty(propertyName) != null;
        }

        public static bool HasProperty(this Type obj, string propertyName)
        {
            return obj.GetProperty(propertyName) != null;
        }
        public static IOrderedQueryable<T> OrderBy<T>(this IQueryable<T> source, string propertyName)
        {
            return OrderingHelper(source, propertyName, false, false);
        }

        public static IOrderedQueryable<T> OrderByDescending<T>(this IQueryable<T> source, string propertyName)
        {
            return OrderingHelper(source, propertyName, true, false);
        }

        public static IOrderedQueryable<T> ThenBy<T>(this IOrderedQueryable<T> source, string propertyName)
        {
            return OrderingHelper(source, propertyName, false, true);
        }

        public static IOrderedQueryable<T> ThenByDescending<T>(this IOrderedQueryable<T> source, string propertyName)
        {
            return OrderingHelper(source, propertyName, true, true);
        }
        private static IOrderedQueryable<T> OrderingHelper<T>(IQueryable<T> source, string propertyName,
           bool descending, bool anotherLevel)
        {
            var param = Expression.Parameter(typeof(T), "p");
            var property = Expression.PropertyOrField(param, propertyName);
            var sort = Expression.Lambda(property, param);

            var call = Expression.Call(
                typeof(Queryable),
                (!anotherLevel ? "OrderBy" : "ThenBy") + (descending ? "Descending" : string.Empty),
                new[] { typeof(T), property.Type },
                source.Expression,
                Expression.Quote(sort));

            return (IOrderedQueryable<T>)source.Provider.CreateQuery<T>(call);
        }

       

        public static IQueryable<T> ApplySorting<T>(this IQueryable<T> dbset, string listSortStr)
        {
            var dataSet = dbset.AsQueryable();
            var orderedDataSet = dataSet as IOrderedQueryable<T>;
            var hasOder = false;
            var listSort = listSortStr.Split(',').ToList();
            for (var i = 0; i < listSort.Count; i++)
            {
                var sortItem = listSort[i];
                if (!string.IsNullOrEmpty(sortItem))
                {
                    sortItem = sortItem.Trim();
                    var sortType = sortItem[0];
                    var propName = sortItem.Substring(1);

                    if (typeof(T).HasProperty(propName))
                    {
                        if (sortType == '+')
                        {
                            orderedDataSet = i == 0
                                ? (orderedDataSet ?? throw new InvalidOperationException()).OrderBy(propName)
                                : (orderedDataSet ?? throw new InvalidOperationException()).ThenBy(propName);
                            hasOder = true;
                        }

                        if (sortType == '-')
                            if (sortType == '-')
                            {
                                orderedDataSet = i == 0
                                    ? (orderedDataSet ?? throw new InvalidOperationException()).OrderByDescending(
                                        propName)
                                    : (orderedDataSet ?? throw new InvalidOperationException()).ThenByDescending(
                                        propName);
                                hasOder = true;
                            }
                    }
                }
            }

            if (hasOder) dataSet = orderedDataSet;
            return dataSet;
        }
        
        public static async Task<Pagination<T>> GetPageAsync<T>(this IQueryable<T> dbSet, PaginationRequest query)
            where T : class
        {
            
            query.Page = query.Page ?? 1;
            if (query.Sort != null && query.Size.HasValue)
            {
                dbSet = dbSet.ApplySorting(query.Sort);
                var totals = await dbSet.CountAsync();

                if (query.Size == -1)
                    query.Size = totals;

                var totalsPages = (int)Math.Ceiling(totals / (float)query.Size.Value);
                var excludedRows = (query.Page.Value - 1) * query.Size.Value;
                var items = await dbSet.Skip(excludedRows).Take(query.Size.Value).ToListAsync();
                return new Pagination<T>
                {
                    Page = query.Page.Value,
                    Content = items,
                    NumberOfElements = items.Count,
                    Size = query.Size.Value,
                    TotalPages = totalsPages,
                    TotalElements = totals
                };
            }

            if (!query.Size.HasValue)
            {
                var totals = await dbSet.CountAsync();
                var items = await dbSet.ToListAsync();
                return new Pagination<T>
                {
                    Page = 1,
                    Content = items,
                    NumberOfElements = totals,
                    Size = totals,
                    TotalPages = 1,
                    TotalElements = totals
                };
            }

            return null;
        }
      
    }
}