﻿using AutoMapper;
using Bands.Business;
using Bands.Common;
using Bands.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bands.AutoMapperConfig
{
    public class AutoMapperConfig : Profile
    {
        public AutoMapperConfig()
        {
            CreateMap(typeof(Pagination<>), typeof(Pagination<>));

            #region Album Mapper
            CreateMap<Album, AlbumViewModel>().ReverseMap();
            CreateMap<AlbumCreatingModel, Album>().ReverseMap();
            CreateMap<AlbumUpdateModel, Album>().ReverseMap();
            #endregion


            #region Band Mapper
            CreateMap<Band, BandViewModel>()
                .ForMember(
                    dest => dest.FoundedYearsAgo,
                    opt => opt.MapFrom(src => $"{src.Founded.ToString("yyyy")} ({src.Founded.GetYearsAgo()}) years ago"));
         
            CreateMap<BandCreatingUpdateModel, Band>().ReverseMap();
            #endregion



        }
    }
}
