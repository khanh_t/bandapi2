﻿using Bands.Common;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Bands.Business
{
   public interface IBandHandler
    {
        Task<Response> CreateBand(BandCreatingUpdateModel param);
        Task<Response> UpdateBand(BandCreatingUpdateModel param, Guid id);
        Task<Response> DeleteBand(Guid id);
        Task<Response> GetById(Guid id);
        Task<Response> GetBands(IEnumerable<Guid> ids);
        Task<Response> CreateBands(IEnumerable<BandCreatingUpdateModel> bands);
        Task<Response> GetPageAsync(BandQueryModel query);
    } 
}
