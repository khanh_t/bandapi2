﻿using AutoMapper;
using Bands.Common;
using Bands.Data;
using LinqKit;
using Microsoft.EntityFrameworkCore;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Bands.Business
{
    public class AlbumHandler : IAlbumHandler
    {
        private readonly DBContext _dbcontext;
        private readonly IMapper _mapper;
        public AlbumHandler(DBContext dbcontext, IMapper mapper)
        {
            _dbcontext = dbcontext ?? throw new ArgumentNullException(nameof(dbcontext));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }
        public async Task<Response> CreateAlbum(AlbumCreatingModel param, Guid bandId)
        {
            try
            {
              
                var album = await _dbcontext.Albums.Where(a => a.Title.ToLower() == param.Title.ToLower()).FirstOrDefaultAsync();
                if (album != null)
                {
                    return new ResponseError(HttpStatusCode.BadRequest, "Album đã có !!");
                }

                album = new Album();
                album.Title = param.Title;
                album.Description = param.Description;
                album.BandId = bandId;

                

                _dbcontext.Albums.Add(album);
                var result = _mapper.Map<AlbumCreatingModel>(album);
                return new ResponseObject<AlbumCreatingModel>(result);
            }
            catch (Exception ex)
            {
                Log.Error(ex, string.Empty);
                Log.Error("Param: {@Param}", param);
                return new Response<AlbumCreatingModel>
                {
                    Data = null,
                    Message = ex.Message,
                    Code = HttpStatusCode.InternalServerError
                };
            }
        }

        public async Task<Response> DeleteAlbum(Guid id,Guid bandId)
        {
           try
            {
                var album = await _dbcontext.Albums.Where(a => a.Id == id && a.BandId == bandId).FirstOrDefaultAsync();
                if (album == null)
                {
                    return new ResponseError(HttpStatusCode.BadRequest, "Album không tồn tại!!");
                }

                _dbcontext.Albums.Remove(album);
             var status =   await _dbcontext.SaveChangesAsync();
                if (status > 0)
                {
                    return new ResponseError(HttpStatusCode.OK, "Xóa album thành công");
                }
                else return new ResponseError(HttpStatusCode.BadRequest, "Xóa album thất bại" );

            } 
            catch(Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(HttpStatusCode.InternalServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }

       

        public async Task<Response> GetById( Guid bandId,Guid id)
        {
            try
            {
                var album = await _dbcontext.Albums.Where(a => a.Id == id && a.BandId==bandId).FirstOrDefaultAsync();
                if (album == null)
                {
                    return new ResponseError(HttpStatusCode.BadRequest, "Album không tồn tại!!");
                }
                var result = _mapper.Map<Album, AlbumViewModel>(album);
                return new ResponseObject<AlbumViewModel>(result);
                
            }
            catch (Exception ex)
            {
                Log.Error(ex, string.Empty);
                Log.Error("Id: {@Id}", id);
                return new Response<AlbumCreatingModel>
                {
                    Data = null,
                    Message = ex.Message,
                    Code = HttpStatusCode.InternalServerError
                };
            }
        }

        public async Task<Response> GetPageAsync(AlbumQueryModel query,Guid bandId)
        {
            try
            {

                var predicate = BuildQuery(query);

                var queryResult = _dbcontext.Albums.Where( predicate);

                

                var data = await queryResult.Where(a => a.BandId == bandId).GetPageAsync(query);

                var result = _mapper.Map<Pagination<Album>, Pagination<AlbumViewModel>>(data);


                if (result != null)
                {

                    return new ResponsePagination<AlbumViewModel>(result);
                }

                return new ResponseError(HttpStatusCode.NotFound, "Không tìm thấy dữ liệu");
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(HttpStatusCode.InternalServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }

        private Expression<Func<Album, bool>> BuildQuery(AlbumQueryModel query)
        {
            var predicate = PredicateBuilder.New<Album>(true);
            return predicate;
        }

        public async Task<Response> UpdateAlbum(AlbumUpdateModel param, Guid id,Guid bandId)
        {
            try
            {
                var album = await _dbcontext.Albums.Where(a => a.Id == id).FirstOrDefaultAsync();
                if (album == null)
                {
                    return new ResponseError(HttpStatusCode.BadRequest, "Album không tồn tại!!");
                }

                album.Title = param.Title;
                album.Description = param.Description;
                album.BandId = bandId;
                
                await _dbcontext.SaveChangesAsync();

                var result = _mapper.Map<Album, AlbumUpdateModel>(album);
                return new ResponseObject<AlbumUpdateModel>(result);
            }
            catch(Exception ex)
            {
                Log.Error(ex, string.Empty);
                Log.Error("Id: {@Id}", id);
                return new ResponseError(HttpStatusCode.InternalServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
            
        }
    }
}
